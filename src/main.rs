pub mod lib;

use clap::{App, Arg};
use failure::Error;
use lib::parse;
use std::fs;
use std::fs::File;
use std::io::BufReader;

fn main() -> Result<(), Error> {
    let matches = App::new("CLI")
        .about("Convert clippy warnings into GitLab Code Quality report")
        .arg(
            Arg::new("input")
                .about("input file, generate it with `cargo clippy --message-format=json &> clippy.txt`")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    if let Some(input) = matches.value_of("input") {
        let file = File::open(input)?;
        let reader = BufReader::new(file);
        let data = parse(reader)?;
        fs::write("gl-code-quality-report.json", data).expect("Unable to write file");
    };
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::read_to_string;
    use std::process::{Command, Stdio};

    #[test]
    fn valid_fixture() {
        let f = File::open("tests/fixtures/valid.txt").expect("Unable to open file");
        let reader = BufReader::new(f);
        let result = parse(reader).unwrap();
        let expected = read_to_string("tests/fixtures/valid.json").expect("Unable to open file");
        assert_eq!(result, expected);
    }

    #[test]
    fn invalid_fixture() {
        let f = File::open("tests/fixtures/invalid.txt").expect("Unable to open file");
        let reader = BufReader::new(f);
        let result = parse(reader);
        assert!(result.is_err());
    }

    #[test]
    fn execute_clippy_locally() {
        let mut command = Command::new("cargo")
            .args(&["clippy", "--message-format=json"])
            .current_dir(".")
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();
        let reader = BufReader::new(command.stdout.take().unwrap());
        parse(reader).unwrap();
        command.wait().expect("Unable to get cargo's exit status");
    }
}

FROM rust:1.46-buster AS builder

WORKDIR /usr/src/app
COPY . .
RUN cargo build --package backend --release

FROM debian:buster-slim

COPY --from=builder /usr/src/app/target/release/backend /usr/local/bin

CMD /bin/bash -c "HOST=0.0.0.0 PORT=$PORT /usr/local/bin/server"

use crate::api::responses::client_error::ClientError;
use gitlab_clippy::parse;
use gotham::handler::{HandlerError, IntoResponse};
use gotham::hyper::body;
use gotham::hyper::body::Buf;
use gotham::hyper::{Body, StatusCode};
use gotham::state::{FromState, State};

pub async fn handle_clippy(state: &mut State) -> Result<impl IntoResponse, HandlerError> {
    let data = body::to_bytes(Body::take_from(state)).await?;
    match parse(data.bytes()) {
        Ok(result) => Ok((StatusCode::OK, mime::APPLICATION_JSON, result)),
        Err(e) => {
            let error = ClientError::new(e, "Unable to parse body");
            let result = serde_json::to_string(&error)?;
            Ok((StatusCode::BAD_REQUEST, mime::APPLICATION_JSON, result))
        }
    }
}

#[cfg(test)]
mod tests {
    use gotham::hyper::Body;
    use gotham::hyper::StatusCode;
    use gotham::test::TestServer;
    use proptest::prelude::*;
    use std::fs::read_to_string;

    use crate::api::router::router;

    fn test_server() -> TestServer {
        TestServer::new(router()).expect("Unable to start test server")
    }

    #[test]
    fn valid_request() {
        let test_server = test_server();
        let body = read_to_string("../tests/fixtures/valid.txt").expect("Unable to open file");
        let response = test_server
            .client()
            .post("http://localhost:3000/clippy", body, mime::TEXT_PLAIN)
            .perform()
            .expect("Unable to send a request");

        assert_eq!(response.status(), StatusCode::OK);
        let body = response.read_body().expect("Unable to get response body");
        let response_body = String::from_utf8(body).expect("Unable to convert bytes to string");
        let expected = read_to_string("../tests/fixtures/valid.json").expect("Unable to open file");
        assert_eq!(response_body, expected);
    }

    #[test]
    fn empty_body() {
        let test_server = test_server();
        let body = "";
        let response = test_server
            .client()
            .post("http://localhost:3000/clippy", body, mime::TEXT_PLAIN)
            .perform()
            .expect("Unable to send a request");

        assert_eq!(response.status(), StatusCode::OK);
        let body = response.read_body().expect("Unable to get response body");
        assert_eq!(body, b"[]");
    }

    #[test]
    fn invalid_body() {
        let test_server = test_server();
        let raw: Vec<u8> = "😀".bytes().collect();
        let body = Body::from(raw[1..4].to_vec());
        let response = test_server
            .client()
            .post("http://localhost:3000/clippy", body, mime::TEXT_PLAIN)
            .perform()
            .expect("Unable to send a request");

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        let body = response.read_body().expect("Unable to get response body");
        let response_body = String::from_utf8(body).expect("Unable to convert bytes to string");
        let expected =
            read_to_string("../tests/fixtures/invalid.json").expect("Unable to open file");
        assert_eq!(response_body, expected);
    }

    proptest! {
        #[test]
        fn fuzz(body in "\\PC*") {
            let test_server = test_server();
            let response = test_server
                .client()
                .post("http://localhost:3000/clippy", body, mime::TEXT_PLAIN)
                .perform()
                .expect("Unable to send a request");

            assert_eq!(response.status(), StatusCode::OK);
            let body = response.read_body().expect("Unable to get response body");
            assert_eq!(body, b"[]");
        }
    }
}
